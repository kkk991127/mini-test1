import React, {useState} from "react";


const TestPage = () => {
    const [manName , setManName] = useState('')
    const [womanName , setWomanName ] = useState('')
    const [score , setScore ] = useState(0)

    const handleManName =  e => {
        setManName(e.target.value)
    }

    const handleWomanName = e => {
        setWomanName(e.target.value)
    }

    const calculateScore = () => {
        setScore(Math.floor(Math.random() * 101))
    }

    return(
        <div>
            <h1>이름 궁합보기</h1>
            <p>페이지1</p>

                <div className="input" >
                <input type="text" value={manName} onChange={handleManName}/>
                </div>
                <div className="input">
                <input type="text" value={womanName} onChange={handleWomanName}/>
                </div>
                <button className="input" onClick={calculateScore}>궁합보기</button>
            <p> {manName}와 {womanName}의 궁합도는 {score}%</p>
        </div>
    )
}

export default TestPage;