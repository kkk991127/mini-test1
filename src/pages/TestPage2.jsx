import React, {useState} from "react";

const TestPage2 = () => {
    const [topOutFit , setTopOutFit] = useState('')
    const [bottomFit , setBottomFit ] = useState('')
    const [pickTop, setPickTop ] = useState('')
    const [pickBottom, setPickBottom ] = useState('')

    const handleTopOutFit = (e) => {
      setTopOutFit(e.target.value)
    }

    const handleBottomFit = (e) => {
        setBottomFit(e.target.value)
    }

    const pickCloset = () => {
        const topArr = topOutFit.split(',')
        const bottomArr = bottomFit.split(',')

        setPickTop(topArr[Math.floor(Math.random() * topArr.length)]);
        setPickBottom(bottomArr[Math.floor(Math.random() * topArr.length)]);
    }


    return (
        <div>
            <h1> 옷 추천 </h1>
            <div className="input2">
                <p>상의</p> <input type="text" value={topOutFit} onChange={handleTopOutFit}/>
            </div>
            <div className="input2">
                <p>하의</p>
                <input type="text" value={bottomFit} onChange={handleBottomFit}/>
            </div>
            <button onClick={pickCloset}>내일 뭐 입지?</button>
            <p> 당신이 내일 입게 될 옷입니다 ! {pickTop} {pickBottom}</p>
            <div>상의: {topOutFit} 하의: {bottomFit}</div>
        </div>

    )
}

export default TestPage2;