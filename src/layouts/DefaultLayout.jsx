import React from "react";
import {Link} from "react-router-dom";

const DefaultLayout = ({children}) => {
    return (
        <>
            <header>헤더입니당</header>
            <Link to="/">테스트1</Link>
            <Link to="/test">테스트2</Link>
            <Link to="/test2">테스트3</Link>
            <Link to="/test3">테스트4</Link>
            <main> {children}</main>
            <footer></footer>
        </>
    )
}

export default DefaultLayout;