import './App.css';
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import TestPage from "./pages/TestPage";
import TestPage2 from "./pages/TestPage2";
import TestPage3 from "./pages/TestPage3";
import TestPage4 from "./pages/TestPage4";

function App() {
  return (
    <div className="App">
  <Routes>
      <Route path="/" element={<DefaultLayout><TestPage/></DefaultLayout>}/>
      <Route path="/test" element={<DefaultLayout><TestPage2></TestPage2></DefaultLayout>}/>
      <Route path="/test2" element={<DefaultLayout><TestPage3/></DefaultLayout>}/>
      <Route path="/test3" element={<DefaultLayout><TestPage4/></DefaultLayout>}/>
  </Routes>
    </div>
  );
}

export default App;
